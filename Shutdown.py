#!/usr/bin/env python


import serial
import pigpio # http://abyz.co.uk/rpi/pigpio/python.html
from ssh import ssh

# I should add a function so that I can pass IP's and select which pi to shutdown
# Something like this:
# def piShutdown(piIps):
# 	for piIp in piIps:
# 		try:           
# 			connection1 = ssh("192.168.1.10" + piIp, "pi", "slave2ai")
# 			connection1.sendCommand("sudo shutdown -h now > /dev/null 2>&1 &")
# 			print("10" + piIp + " off")
# 		except:
# 			pass

# For the Pi using halt is the same as shutdown. Not true on other linux/UNIX boxws.
# I should probably change halt to shutdown at some point
try:           
	connection0 = ssh("192.168.1.101", "dai", "slave2ai")
	connection0.sendCommand("sudo shutdown -h now > /dev/null 2>&1 &")
	print("Nano 101 off")
except:
	pass
try:           
	connection1 = ssh("192.168.1.102", "dai", "slave2ai")
	connection1.sendCommand("sudo shutdown -h now > /dev/null 2>&1 &")
	print("Nano 102 off")
except:
	pass
try:
	connection2 = ssh("192.168.1.103", "dai", "slave2ai")
	connection2.sendCommand("sudo shutdown -h now > /dev/null 2>&1 &")
	print("Nano 103 off")
except:
	pass
try:
	connection3 = ssh("192.168.1.111", "pi", "slave2ai")
	connection3.sendCommand("sudo shutdown -h now > /dev/null 2>&1 &")
	print("111 off")
except:
	pass
try:
	connection4 = ssh("192.168.1.112", "pi", "slave2ai")
	connection4.sendCommand("sudo shutdown -h now > /dev/null 2>&1 &")
	print("112 off")
except:
	pass
try:
	connection5 = ssh("192.168.1.113", "pi", "slave2ai")
	connection5.sendCommand("sudo shutdown -h now > /dev/null 2>&1 &")
	print("113 off")
except:
	pass
# The following one is for the JetsonTX2, it needs to have NOPASSWD added to sudo using sudo visudo
try:
	connection6 = ssh("localhost", "nvidia", "nvidia")
	print("Goodbye world")
	connection6.sendCommand("sudo shutdown now > /dev/null 2>&1 &")
except:
	pass