import serial

def serialConnect(): 
    serlocations=['/dev/ttyUSB0', '/dev/ttyUSB1','/dev/ttyUSB2','/dev/ttyUSB3', '/dev/ttyUSB4', '/dev/ttyUSB5', '/dev/ttyUSB6', '/dev/ttyUSB7', '/dev/ttyUSB8', '/dev/ttyUSB9', '/dev/ttyUSB10', 'end']
    for device in serlocations:
        try:
            ser = serial.Serial(
                port=device,
                baudrate=9600,
                parity=serial.PARITY_ODD,
                stopbits=serial.STOPBITS_TWO,
                bytesize=serial.SEVENBITS
            )
            print(device)
            return ser  
        except:
            x=0 
    if device == 'end':
        print("No Device Found")  

ser = serialConnect()
if ser:
    ser.write("TEST")
    ser.timeout=5
    for i in ser.readlines():
        print(i)