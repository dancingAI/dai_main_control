import time
from pypozyx import *

from Performance import Performance
import serial



class InertiaMeter:

    def __init__(self, performance: Performance):
        
        # self._serial_port = get_first_pozyx_serial_port()
        
        # if self._serial_port is not None:
        #     self._pozyx = PozyxSerial(self._serial_port)
        # else:
        #     print("No Pozyx port was found")
       

        self._anchors = [DeviceCoordinates(0x610a, 1, performance.inertiaMeterBeacon1),
                        DeviceCoordinates(0x6110, 1, performance.inertiaMeterBeacon2),
                        DeviceCoordinates(0x611c, 1, performance.inertiaMeterBeacon3),
                        DeviceCoordinates(0x611a, 1, performance.inertiaMeterBeacon4),]
						# DeviceCoordinates(0x683b, 1, performance.inertiaMeterBeacon5),
						# DeviceCoordinates(0x6826, 1, performance.inertiaMeterBeacon6),
						# DeviceCoordinates(0x682e, 1, performance.inertiaMeterBeacon7),
						# DeviceCoordinates(0x683a, 1, performance.inertiaMeterBeacon8),]

        self._initialisePozyx()

        
    def _initialisePozyx(self): 
         
       
        self._serial_port = get_first_pozyx_serial_port()
        self._pozyx = PozyxSerial(self._serial_port)
        print(self._serial_port)

        self._pozyx.clearDevices()             
        self._setAnchorsManual()
        self._pozyx.setPositionAlgorithmNormal() # setPositionAlgorithmTracking()
        self._pozyx.setPositioningFilterMovingAverage(8)
        self._pozyx.setRangingProtocolPrecision() # setRangingProtocolFast()

        # self._pozyx.setUpdateInterval(100)
        
       
       
        print("done initialising")

    def _setAnchorsManual(self):
        self._status = self._pozyx.clearDevices(None)
        print("Anchors cleared: ", self._status)
        for self._anchor in self._anchors:
            self._status &= self._pozyx.addDevice(self._anchor, None)
            print("status:", self._status)
        # if len(self._anchors) > 4:
            # self._status &= self._pozyx.setSelectionOfAnchors(POZYX_ANCHOR_SEL_AUTO, len(self._anchors))      
            # self._pozyx.getNumberOfAnchors(nbr_anchors,None)
            # print("length" , ord(nbr_anchors[0]))
        # print(self._status)
        return self._status

    @property
    def currentSettings(self):
        try:
            self._uwb_settings = UWBSettings()
            self._pozyx.getUWBSettings(self._uwb_settings)
            return(self._uwb_settings)
        except:
            raise Exception("Pozyx UWBSettings could not be retrieved")
    
    @property
    def position(self)->Coordinates:
        try:
            #self._pozyx.ser.reset_input_buffer()
                    
            self._position = Coordinates()
            #for i in range(10):
            self._status = self._pozyx.doPositioning(self._position)
            if self._status == POZYX_SUCCESS:
                return self._position
            else:
                print("Trying to reconnect to POZYX", POZYX_SUCCESS)
                time.sleep(1)
                self._initialisePozyx()
                self._position = Coordinates()
                self._status = self._pozyx.doPositioning(self._position)
                if self._status == POZYX_SUCCESS:
                    return self._position                           
                 
        except:
            raise Exception("position is not defined")
            
            
    @property
    def linearAcceleration(self)->Acceleration:
        try:
            self._linear_acceleration = Acceleration()
            self._status = self._pozyx.getAcceleration_mg(self._linear_acceleration, None)
            if self._status == POZYX_SUCCESS:
                #print("found device", self._serial_port)
                return self._linear_acceleration
                
            else:                           
                self._initialisePozyx()
                self._linear_acceleration = Acceleration()
                self._status = self._pozyx.getAcceleration_mg(self._linear_acceleration, None)
                if self._status == POZYX_SUCCESS:
                    #print("found device", self._serial_port)
                    return self._linear_acceleration                  
                                   
        except:
            raise Exception("linear_acceleration is not defined")
            
    @property
    def angularPosition(self)->EulerAngles:
        try:
            self._angular_position = EulerAngles()
            self._status = self._pozyx.getEulerAngles_deg(self._angular_position, None)
            if self._status == POZYX_SUCCESS:
                #print("found device", self._serial_port)
                return self._angular_position
                
            else:               
                self._initialisePozyx()
                self._angular_position = EulerAngles()
                self._status = self._pozyx.getEulerAngles_deg(self._angular_position, None)
                if self._status == POZYX_SUCCESS:
                    #print("found device", self._serial_port)
                    return self._angular_position                   
                
        except:
            raise Exception("angular_position is not defined")

    @property
    def angularPositionQuaternion(self)->Quaternion:
        try:
            self.angular_position_quaternion = Quaternion()
            self._status = self._pozyx.getQuaternion(self.angular_position_quaternion, None)
            if self._status == POZYX_SUCCESS:
                #print("found device", self._serial_port)
                return self.angular_position_quaternion
                
            else:                
                self._initialisePozyx()
                self.angular_position_quaternion = Quaternion()
                self._status = self._pozyx.getQuaternion(self.angular_position_quaternion, None)
                if self._status == POZYX_SUCCESS:
                    #print("found device", self._serial_port)
                    return self.angular_position_quaternion                  
                
        except:
            raise Exception("angular_position is not defined")			
			
            
    @property
    def angularVelocity(self)->AngularVelocity:
        try:
            self._angular_velocity = AngularVelocity()
            self._status = self._pozyx.getAngularVelocity_dps(self._angular_velocity, None)
            if self._status == POZYX_SUCCESS:
                return self._angular_velocity
 
            else:                
                self._initialisePozyx()
                self._angular_velocity = AngularVelocity()
            self._status = self._pozyx.getAngularVelocity_dps(self._angular_velocity, None)
            if self._status == POZYX_SUCCESS:
                return self._angular_velocity                     
        except:
            raise Exception("angular_velocity is not defined")
            
   
    @property
    def magneticOrientation(self)->Magnetic:
        try:
            self._magnetic_orientation = Magnetic()
            self._status = self._pozyx.getMagnetic_uT(self._magnetic_orientation, None)
            if self._status == POZYX_SUCCESS:
                #print("found device", self._serial_port)
                return self._magnetic_orientation
                
            else:               
                self._initialisePozyx()
                self._magnetic_orientation = Magnetic()
                self._status = self._pozyx.getMagnetic_uT(self._magnetic_orientation, None)
                if self._status == POZYX_SUCCESS:
                    #print("found device", self._serial_port)
                    return self._magnetic_orientation                   
                
        except:
            raise Exception("angular_position is not defined")      
