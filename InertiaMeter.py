import time
import threading
import sys
from pypozyx import *

from Performance import Performance
import serial



class InertiaMeter:

    def __init__(self, performance: Performance):
        
        # self._serial_port = get_first_pozyx_serial_port()
        
        # if self._serial_port is not None:
        #     self._pozyx = PozyxSerial(self._serial_port)
        # else:
        #     print("No Pozyx port was found")
       

        self._position = Coordinates()
        self._linear_acceleration = Acceleration()
        self._angular_position = EulerAngles()
        self.angular_position_quaternion = Quaternion()
        self._angular_velocity = AngularVelocity()
        self._magnetic_orientation = Magnetic()
        
        self._status = POZYX_SUCCESS

        self._anchors = [DeviceCoordinates(0x610a, 1, performance.inertiaMeterBeacon1),
                        DeviceCoordinates(0x6110, 1, performance.inertiaMeterBeacon2),
                        DeviceCoordinates(0x611c, 1, performance.inertiaMeterBeacon3),
                        DeviceCoordinates(0x611a, 1, performance.inertiaMeterBeacon4),]
                        # DeviceCoordinates(0x683b, 1, performance.inertiaMeterBeacon5),
                        # DeviceCoordinates(0x6826, 1, performance.inertiaMeterBeacon6),
                        # DeviceCoordinates(0x682e, 1, performance.inertiaMeterBeacon7),
                        # DeviceCoordinates(0x683a, 1, performance.inertiaMeterBeacon8),]

        self._initialisePozyx()

        self._lock = threading.Lock()
        self._update_lock = threading.Lock()
        t = threading.Thread(target=self.async_update, daemon=True)
        t.start()
        

    def _initialisePozyx(self): 
         
       
        self._serial_port = get_first_pozyx_serial_port()
        self._pozyx = PozyxSerial(self._serial_port)
        print(self._serial_port)

        self._pozyx.clearDevices()             
        self._setAnchorsManual()
        self._pozyx.setPositionAlgorithmNormal() # setPositionAlgorithmTracking()
        self._pozyx.setPositioningFilterMovingAverage(8)
        self._pozyx.setRangingProtocolPrecision() # setRangingProtocolFast()

        # self._pozyx.setUpdateInterval(100)
              
        print("done initialising")


    def _setAnchorsManual(self):
        self._status = self._pozyx.clearDevices(None)
        print("Anchors cleared: ", self._status)
        for self._anchor in self._anchors:
            self._status &= self._pozyx.addDevice(self._anchor, None)
            print("status:", self._status)
        # if len(self._anchors) > 4:
            # self._status &= self._pozyx.setSelectionOfAnchors(POZYX_ANCHOR_SEL_AUTO, len(self._anchors))      
            # self._pozyx.getNumberOfAnchors(nbr_anchors,None)
            # print("length" , ord(nbr_anchors[0]))
        # print(self._status)
        return self._status


    @property
    def currentSettings(self):
        try:
            self._uwb_settings = UWBSettings()
            self._pozyx.getUWBSettings(self._uwb_settings)
            return(self._uwb_settings)
        except:
            raise Exception("Pozyx UWBSettings could not be retrieved")
    

    def async_update(self):
        while True:
            try:
                self.async_position_update()
                self.async_linearAcceleration_update()
                self.async_angularPosition_update()
                self.async_angularPositionQuaternion_update()
                self.async_angularVelocity_update()
                self.async_magneticOrientation_update()
            except Exception as ex:
                print("ERROR RETRIEVING IMT INFO: ", ex)
                print("Trying to reconnect to POZYX")
                time.sleep(1)
                self._initialisePozyx()


    @property
    def position(self)->Coordinates:
        try:
            with self._update_lock:
                return self._position                           
        except:
            raise Exception("position is not defined")
    

    def async_position_update(self):
        try:
            new_pos = Coordinates()
            with self._lock:
                self._status = self._pozyx.doPositioning(new_pos)
                if self._status == POZYX_SUCCESS:
                    # print("thread position: ", new_pos.x, new_pos.y, new_pos.z)
                    x_pos = new_pos.x
                    y_pos = new_pos.y
                    z_pos = new_pos.z
                    with self._update_lock:
                            self._position = new_pos
                else:
                    print("problem retrieving position")                   
        except Exception as ex:
            raise Exception("error retrieving position: ", ex)

            
    @property
    def linearAcceleration(self)->Acceleration:
        try:
            with self._update_lock:
                return self._linear_acceleration                           
        except:
            raise Exception("linear_acceleration is not defined")


    def async_linearAcceleration_update(self):
        try:
            with self._lock:
                new_acc = Acceleration()
                self._status = self._pozyx.getAcceleration_mg(new_acc, None)
                if self._status == POZYX_SUCCESS:
                    x_acc = new_acc.x
                    y_acc = new_acc.y
                    z_acc = new_acc.z
                    with self._update_lock:
                        self._linear_acceleration =  new_acc
                else:
                    print("problem retrieving linearAcceleration")                   
        except Exception as ex:
            raise Exception("error retrieving linear_acceleration: ", ex)


    @property
    def angularPosition(self)->EulerAngles:
        try:
            with self._update_lock:
                return self._angular_position                                           
        except:
            raise Exception("angular_position is not defined")


    def async_angularPosition_update(self):
        try:
            with self._lock:
                new_aPos = EulerAngles()
                self._status = self._pozyx.getEulerAngles_deg(new_aPos, None)
                if self._status == POZYX_SUCCESS:
                    h_aPos = new_aPos.heading
                    r_aPos = new_aPos.roll
                    p_aPos = new_aPos.pitch
                    with self._update_lock:
                        self._angular_position =  new_aPos                   
                else:
                    print("problem retrieving angularPosition")   
        except Exception as ex:
            raise Exception("error retrieving angular_position: ", ex)


    @property
    def angularPositionQuaternion(self)->Quaternion:
        try:
            with self._update_lock:
                return self.angular_position_quaternion                                           
        except:
            raise Exception("angular_position_quaternion is not defined")


    def async_angularPositionQuaternion_update(self):
        try:
            with self._lock:
                new_aPos = Quaternion()
                self._status = self._pozyx.getQuaternion(new_aPos, None)
                if self._status == POZYX_SUCCESS:
                    x_aPos = new_aPos.x
                    y_aPos = new_aPos.y
                    z_aPos = new_aPos.z
                    w_aPos = new_aPos.w
                    with self._update_lock:
                        self.angular_position_quaternion =  new_aPos                   
                else:
                    print("problem retrieving angularPositionQuaternion")   
        except Exception as ex:
            raise Exception("error retrieving angular_position_quaternion: ", ex)



    @property
    def angularVelocity(self)->AngularVelocity:
        try:
            with self._update_lock:
                return self._angular_velocity                                           
        except:
            raise Exception("angular_velocity is not defined")
            

    def async_angularVelocity_update(self):
        try:
            with self._lock:
                new_avel = AngularVelocity()
                self._status = self._pozyx.getAngularVelocity_dps(new_avel, None)
                if self._status == POZYX_SUCCESS:
                    x_avel = new_avel.x
                    y_avel = new_avel.y
                    z_avel = new_avel.z
                    with self._update_lock:
                        self._angular_velocity = new_avel                   
                else:
                    print("problem retrieving angularVelocity")   
        except Exception as ex:
            raise Exception("error retrieving angular_velocity: ", ex)


    @property
    def magneticOrientation(self)->Magnetic:
        try:
            with self._update_lock:
                return self._magnetic_orientation                                           
        except:
            raise Exception("magnetic_orientation  is not defined")


    def async_magneticOrientation_update(self):
        try:
            with self._lock:
                new_mo = Magnetic()
                self._status = self._pozyx.getMagnetic_uT(new_mo, None)
                if self._status == POZYX_SUCCESS:
                    x_mo = new_mo.x
                    y_mo = new_mo.y
                    z_mo = new_mo.z
                    with self._update_lock:
                        self._magnetic_orientation = new_mo                   
                else:
                    print("problem retrieving magneticOrientation")   
        except Exception as ex:
            raise Exception("error retrieving magnetic_orientation: ", ex)