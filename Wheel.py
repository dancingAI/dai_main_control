#!/usr/bin/env python

import time

import pigpio
import math
import serial


# Naming: Wheels: 1a,1b,2a,2b,3a,3c
# Pi2a controls Wheels 
# Pi2b controls Wheels
# Pi3a controls Wheels


class Wheel:
    def __init__(self, wheel_id):
        
        #775 motor with 64:1 gear ratio
        self._motor_stall_torque = 1.18 * 64 #64 is the gear ratio
        self._motor_free_current = 2.7
        self._motor_stall_current = 135
        self._motor_actual_current = 0
       
        
        
        self._wheel_id = wheel_id
             
        self._hostname = {"1a": "192.168.1.111","1b": "192.168.1.111", "2a": "192.168.1.112","2b": "192.168.1.112", "3a": "192.168.1.113","3b": "192.168.1.113"}
        self._motor_id = {"1a": "1","1b": "2", "2a": "3","2b": "4", "3a": "5","3b": "6"}
              
          
        self._target_speed = 0
        self._freewheel_mode = 0
        
        self._max_rpm = 17040/64 # motor peak rpm / gear ratio / 266.25
        self._max_speed = self._max_rpm * (math.pi/30) # max speed in radians per second / 27.8816
        try:      
            self._pi = pigpio.pi(self._hostname[self._wheel_id],8888)
        except Exception as e: print(e)

        try:      
            self._motor = self._pi.serial_open("/dev/ttyS0", 115200) #for usb : /dev/tty-Wheels
            #print("connected")
        except Exception as e: print(e)
        
        
        

    def closeSerial(self):
        self._pi.serial_close(self._motor)
    
    @property
    def targetSpeed(self):
        try:
            return self._target_speed
        except:
            raise Exception("target_speed not defined")
            
    @targetSpeed.setter
    def targetSpeed(self, target_speed_rad):
        self._target_speed = target_speed_rad
        if -self._max_speed <= self._target_speed <= self._max_speed:
            self._target_speed_serial = str(int(1500 * (self._target_speed / self._max_speed))) #1500 is the speed at which the sabertooth sends 18v
            self._speed_command_serial = "M" + self._motor_id[self._wheel_id] + ":" + self._target_speed_serial + "\r\n"
            self._pi.serial_write(self._motor, self._speed_command_serial)
            # print("wheel target speed:", self._speed_command_serial)
        else:
            print("problem")
            
    @property
    def freewheelMode(self):
        try:
            return self._freewheel_mode
        except:
            raise Exception("freewheel_mode not defined")
            
    @freewheelMode.setter
    def freewheelMode(self, freewheel_mode):
        self._freewheel_mode = freewheel_mode
        self._freewheel_command_serial = "Q" + self._motor_id[self._wheel_id] + ":" + str(self._freewheel_mode) + "\r\n"
        self._pi.serial_write(self._motor, self._freewheel_command_serial)
        
    @property
    def wheelIsOnGround(self):
        try:
            return self._wheel_is_on_ground
        except:
            raise Exception("wheel_is_on_ground not defined")
            
    @wheelIsOnGround.setter
    def wheelIsOnGround(self, wheel_is_on_ground_state):
        self._wheel_is_on_ground = wheel_is_on_ground_state

    @property
    def currentTorque(self):
        try:
            self._get_current_command_serial = "M" + self._motor_id[self._wheel_id] + ":" + "getb\r\n"
            self._actual_current = self._pi.serial_write(self._motor, self._get_current_command_serial)
            #self._actual_current = self._actual_current[1:]
            #print(self._actual_current)
            self._current_torque = (int(self._actual_current) - self._motor_free_current) / ((self._motor_stall_current - self._motor_free_current) / self._motor_stall_torque)
            
            return self._current_torque
        except:
            raise Exception("Could not get current_torque")
            
    def timeout(self, timeout):
        self._timeout = timeout
        self._timeout_command_serial = "Q" + self._motor_id[self._wheel_id] + ":" + str(self._freewheel_mode) + "\r\n"
        self._motor = self._pi.serial_open("/dev/ttyS0", 115200, 0) # /dev/tty-Wheels
        self._pi.serial_write(self._motor, self._freewheel_command_serial)
        self._pi.serial_close(self._motor)
        # print(self._freewheel_command_serial)


