from Wheel import Wheel
import time

wheel1a = Wheel("1a")
wheel2a = Wheel("2a")
wheel3a = Wheel("3a")
wheel1b = Wheel("1b")
wheel2b = Wheel("2b")
wheel3b = Wheel("3b")
for i in range(2):
    # wheel1a.targetSpeed = 10
    # wheel2a.targetSpeed = 10
    # wheel3a.targetSpeed = 10
    wheel1b.targetSpeed = 10
    wheel2b.targetSpeed = 10
    wheel3b.targetSpeed = 10

    time.sleep(5)

# for i in range(2):
    # wheel1a.targetSpeed = -10
    # wheel2a.targetSpeed = -10
    # wheel3a.targetSpeed = -10
    # wheel1b.targetSpeed = -10
    # wheel2b.targetSpeed = -10
    # wheel3b.targetSpeed = -10

    # time.sleep(1)

wheel1a.targetSpeed = 0
wheel2a.targetSpeed = 0
wheel3a.targetSpeed = 0
wheel1b.targetSpeed = 0
wheel2b.targetSpeed = 0
wheel3b.targetSpeed = 0

wheel1a.closeSerial()
wheel2a.closeSerial()
wheel3a.closeSerial()
wheel1b.closeSerial()
wheel2b.closeSerial()
wheel3b.closeSerial()