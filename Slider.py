#!/usr/bin/env python

import time
import pigpio
import serial




class Slider:

    def __init__(self, slider_id, controller_connection):

        # Sliders 1,2 on Sabertooth SER=16003F5D76EA
        # Slider 3 on Sabertooth SER=16006D4CE9D9
        
        self._slider_id = slider_id
        self._controller_connection = controller_connection
        self._slider_position = 0
        self._current_target_speed = 0
             

        # initialize slider position
        self._start_slider = self._slider_id + ",start" + "\r\n"
        self._controller_connection.write_cmd(self._start_slider)
        
        self._home_slider = self._slider_id + ",home" + "\r\n"
        self._controller_connection.write_cmd(self._home_slider)
        time.sleep(5)        
        
    

    def _set_target_speed(self, target_speed_mm_s):
        
        try:
            kangaroo_speed = target_speed_mm_s * 1 
            self._slider_cmd = self._slider_id + ",s" + str(int(kangaroo_speed)) + "\r\n"
             
           
            try:                
                self._controller_connection.write_cmd(self._slider_cmd)
                print("slider target speed command : ", self._slider_cmd)
                print("set_target_speed successful for Slider: ", self._slider_id)
            except:
                print("set_target_speed failed for Slider: ", self._slider_id)
                              
            self._current_target_speed = target_speed_mm_s      
            print("slider target speed :", kangaroo_speed, self._slider_cmd)
        except:
            raise Exception("Couldn't set target_speed")

    @property
    def targetSpeed(self):
        try:
            return self._current_target_speed
        except:
            raise Exception("No target speed defined")
             
    @targetSpeed.setter
    def targetSpeed(self, target_speed_mm_s):
        self._set_target_speed(target_speed_mm_s)

 
   
    @property
    def sliderPosition(self):
        try:
            
            timer_start = time.time()
            
            kangaroo_pos = ''
            
            self._slider_cmd = self._slider_id + ",getp" + "\r\n"
                      
            read_data = self._controller_connection.read_cmd(self._slider_cmd)
            
            position_list = str(read_data[1],'utf-8').split("\n")
            for x in position_list :
                if x.startswith(self._slider_id) and x.endswith("\r"):
                    kangaroo_pos = x.lower().replace(self._slider_id + ",p","")                   
                    print("kangaroo pos", kangaroo_pos)
                    break         

            timer_end = time.time()
            print("Sliders Serial Read duration: ", timer_end-timer_start)
           
            self._slider_position = int(float(kangaroo_pos)) - 300
            return self._slider_position
            
        except:
            print("Error retrieving slider position: ", kangaroo_pos)
            return 0
            
        
    @property
    def stopperProximityLevel(self):
        try:
            if self._slider_position > 289:
                return (300 - self._slider_position)/10
            elif self._slider_position < -289:
                return (-300 - self._slider_position)/10
            else:
                return 1
        except:
            raise Exception("Slider position is not defined")

    

