#!/usr/bin/env python

from Wheel import Wheel
from Slider2 import Slider2
from InertiaMeter import InertiaMeter
import Performances
from pypozyx import *
import time
import random

print("starting")

w1a = Wheel("1a")
w2a = Wheel("2a")
w3a = Wheel("3a")
w1b = Wheel("1b")
w2b = Wheel("2b")
w3b = Wheel("3b")

w1a.targetSpeed = 0
w2a.targetSpeed = 0
w3a.targetSpeed = 0
w1b.targetSpeed = 0
w2b.targetSpeed = 0
w3b.targetSpeed = 0

# S1=Slider2("1")
# S2=Slider2("2")
# S3=Slider2("3")

# S1.targetSpeed = 100
# S2.targetSpeed = 100
# S3.targetSpeed = 100
# time.sleep(3)
# S1.targetSpeed = 0
# S2.targetSpeed = 0
# S3.targetSpeed = 0

Pos = InertiaMeter(Performances.FluxumPerformance)

Angular_Pos_Prev = EulerAngles()
Angular_Pos_Cur = EulerAngles()

sample_Interval = 0.2
first_Run = True

# In its lowest position (smallest rotation diameter):
# At speed == 5 a full rotation takes approx.  9.8s or 49 x 0.2s intervals
# At speed == 10 a full rotation takes approx. 4.8s or 24 x 0.2s intervals
# At speed == 15 a full rotation takes approx. 3.2s or 16 x 0.2s intervals

for i in range (1,20):
    w1a.targetSpeed = 5
    w2a.targetSpeed = 5
    w3a.targetSpeed = 5
    w1b.targetSpeed = 5
    w2b.targetSpeed = 5
    w3b.targetSpeed = 5

    if first_Run is True:
        time.sleep(sample_Interval)
        Angular_Pos_Cur = Pos.angularPosition
    else:
        Angular_Pos_Prev = Angular_Pos_Cur
        Angular_Pos_Cur = Pos.angularPosition
    print("Current Heading :", Angular_Pos_Cur.heading, " / Previous Heading: ", Angular_Pos_Prev.heading)
    # if ((Angular_Pos_Cur.heading - Angular_Pos_Prev.heading) > 50 and first_Run is False) or ((Angular_Pos_Cur.heading - Angular_Pos_Prev.heading) < -50 and first_Run is False):
    #     if Angular_Pos_Prev.heading > Angular_Pos_Cur.heading:
    #         print("Previous Heading : ", Angular_Pos_Prev.heading, "\tCurrent Heading : ", Angular_Pos_Cur.heading)
    #         print("Heading difference : ", 360 - Angular_Pos_Prev.heading + Angular_Pos_Cur.heading)
    #     else:
    #         print("Previous Heading : ", Angular_Pos_Prev.heading, "\tCurrent Heading : ", Angular_Pos_Cur.heading)
    #         print("Heading difference : ", Angular_Pos_Cur.heading - Angular_Pos_Prev.heading)
    if first_Run == False:
        time.sleep(sample_Interval)
    first_Run = False

w1a.targetSpeed = 0
w2a.targetSpeed = 0
w3a.targetSpeed = 0
w1b.targetSpeed = 0
w2b.targetSpeed = 0
w3b.targetSpeed = 0

time.sleep(1)

w1a.closeSerial()
w2a.closeSerial()
w3a.closeSerial()
w1b.closeSerial()
w2b.closeSerial()
w3b.closeSerial()
# S1.closeSerial()
# S2.closeSerial()
# S3.closeSerial()