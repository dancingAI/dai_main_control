from ControlInput import ControlInput
from ControlOutput import ControlOutput
import time

class ScriptedControlModule():
    
    def __init__(self, train: bool): 
        print("Init")

    def initialize_run(self, episode: int, control_input: ControlInput):       
        print("Initializing run")

    def get_action(self, elpased_time: float) -> ControlOutput:
        outputState = ControlOutput()
        
        firstRun = 1

        outputState.Slider1TargetSpeed = 0
        outputState.Slider2TargetSpeed = 0
        outputState.Slider3TargetSpeed = 0
        
        # Set initial slider height
        # if firstRun == True:
        #     outputState.Slider1TargetSpeed = 100
        #     time.sleep(3)
        #     firstRun = 0

        # 1a-2a-3a
        outputState.Wheel1aTargetSpeed = 0.0
        outputState.Wheel2aTargetSpeed = 8.0
        outputState.Wheel3aTargetSpeed = -8.0

        # # 1b-2a-3a
        # outputState.Wheel1bTargetSpeed = 0.0
        # outputState.Wheel2aTargetSpeed = 8.0
        # outputState.Wheel3aTargetSpeed = -8.0
        
        # # 1a-2b-3a
        # outputState.Wheel1aTargetSpeed = 0.0
        # outputState.Wheel2bTargetSpeed = 8.0
        # outputState.Wheel3aTargetSpeed = -8.0
        
        # # 1b-2b-3a
        # outputState.Wheel1bTargetSpeed = 0.0
        # outputState.Wheel2bTargetSpeed = 8.0
        # outputState.Wheel3aTargetSpeed = -8.0

        # # 1a-2a-3b
        # outputState.Wheel1aTargetSpeed = 0.0
        # outputState.Wheel2aTargetSpeed = 8.0
        # outputState.Wheel3bTargetSpeed = -8.0

        # # 1b-2a-3b
        # outputState.Wheel1bTargetSpeed = 0.0
        # outputState.Wheel2aTargetSpeed = 8.0
        # outputState.Wheel3bTargetSpeed = -8.0

        # # 1a-2b-3b
        # outputState.Wheel1aTargetSpeed = 0.0
        # outputState.Wheel2bTargetSpeed = 8.0
        # outputState.Wheel3bTargetSpeed = -8.0

        # # 1b-2b-3b
        # outputState.Wheel1bTargetSpeed = 0.0
        # outputState.Wheel2bTargetSpeed = 8.0
        # outputState.Wheel3bTargetSpeed = -8.0
        
        return outputState

        
        
        
        
        
        
        
        
                            

    def update_run_results(self, run: int, elapsed_time:float, control_state: ControlInput, control_action: ControlOutput, reward: float, terminated: bool):
        print("Updating run results")

    def finalize_run(self, run: int):       
        print("Finalizing run")






