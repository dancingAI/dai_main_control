from Performance import Performance
from pypozyx import Coordinates

# GillesJobinRehearsalPerformance = Performance([Coordinates(-6710, -4335, 2330),
                                    # Coordinates(6710, -4400, 2525),
                                    # Coordinates(6675, 4400, 1690),
                                    # Coordinates(-6675, 4335, 1940),
                                    # Coordinates(-12810, -3805, 200),
                                    # Coordinates(12730, 3005, 200),
                                    # Coordinates(13210, 5405, 100),
                                    # Coordinates(13210, 5405, 100)],
                                    # 9.0,
                                    # 6.0,
                                    # 60.0)
									
# TinguelyRehearsalPerformance = Performance([Coordinates(-12810, -5405, 5130),
                                    # Coordinates(12810, -5405, 7130),
                                    # Coordinates(12810, 5405, 8130),
                                    # Coordinates(-12810, 5405, 6130),
                                    # Coordinates(-13310, -5405, 150),
                                    # Coordinates(13210, -5405, 100),
                                    # Coordinates(13210, 5405, 100),
                                    # Coordinates(-13310, 5405, 150)],
                                    # 10.0,
                                    # 6.0,
                                    # 60.0)
									
# BolexRehearsalPerformance = Performance([Coordinates(-6593, -3983, 1790),
                                    # Coordinates(6593, -3992, 2257),
                                    # Coordinates(6528, 3992, 1030),
                                    # Coordinates(-6528, 3983, 1468)],
                                    # 10.0,
                                    # 5.0,
                                    # 60.0) 
# ScienceGalleryDublinPerformance = Performance([Coordinates(-5120, -5314, 1700),
                                    # Coordinates(5120, -4160, 1150),
                                    # Coordinates(4829, 4160, 450),
                                    # Coordinates(-4829, 5314, 2360)],
                                    # 7.0,
                                    # 6.0,
                                    # 60.0)
# CampusBiotechPerformance = Performance([Coordinates(-6750, -6800, 130),
                                    # Coordinates(6750, -6800, 1500),
                                    # Coordinates(6750, 6800, 2300),
                                    # Coordinates(-6750, 6800, 1000)],
                                    # 4.0,
                                    # 4.0,
                                    # 60.0)
									
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y (!size/2) and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# Bolex2RehearsalPerformance = Performance([Coordinates(-3890, -3665, 1500),
                                    # Coordinates(3890, -3670, 1000),
                                    # Coordinates(3900, 3670, 130),
                                    # Coordinates(-3900, 3665, 2300)],
                                    # 1.0,
                                    # 1.0,
                                    # 60.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y (!size/2) and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# Bolex2RehearsalPerformance = Performance([Coordinates(-3940, -3665, 1500),
                                    # Coordinates(3940, -3665, 1000),
                                    # Coordinates(3940, 3665, 130),
                                    # Coordinates(-3940, 3665, 2300),],
									# # Coordinates(-4325, 0, 1800),
									# # Coordinates(0, -3980, 2000),
									# # Coordinates(4385, -130, 1700),
									# # Coordinates(0, 3880, 850)],
                                    # 6.0,
                                    # 6.0,
                                    # 60.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y (!size/2) and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# GreyOrangePerformance = Performance([Coordinates(-3940, -6545, 2140),
                                    # Coordinates(5440, -6605, 740),
                                    # Coordinates(5440, 6605, 2120),
                                    # Coordinates(-5440, 6235, 820),],
									# # Coordinates(-4325, 0, 1800),
									# # Coordinates(0, -3980, 2000),
									# # Coordinates(4385, -130, 1700),
									# # Coordinates(0, 3880, 850)],
                                    # 7.0,
                                    # 7.0,
                                    # 60.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y (!size/2) and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# NSDPerformance = Performance([Coordinates(6063, 5295, 1100),
                                    # Coordinates(-6063, 5295, 1565),
                                    # Coordinates(-6063, -9295, 2130),
                                    # Coordinates(6063, -9295, 2410),],
									# # Coordinates(-4325, 0, 1800),
									# # Coordinates(0, -3980, 2000),
									# # Coordinates(4385, -130, 1700),
									# # Coordinates(0, 3880, 850)],
                                    # 6.0,
                                    # 5.0,
                                    # 60.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y (!size/2) and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# NIDPerformance = Performance([Coordinates(-4295, -10331, 2000),
                                    # Coordinates(4295, -10046, 1752),
                                    # Coordinates(4295, 10046, 268),
                                    # Coordinates(-4295, 10331, 500),],
									# # Coordinates(-4325, 0, 1800),
									# # Coordinates(0, -3980, 2000),
									# # Coordinates(4385, -130, 1700),
									# # Coordinates(0, 3880, 850)],
                                    # 8.0,
                                    # 12.0,
                                    # 60.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y (!size/2) and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# CourtyardPerformance = Performance([Coordinates(-6500, -5600, 1515),
                                    # Coordinates(4500, -5600, 2000),
                                    # Coordinates(4500, 4400, 1000),
                                    # Coordinates(-6500, 4400, 480),],
									# # Coordinates(-4325, 0, 1800),
									# # Coordinates(0, -3980, 2000),
									# # Coordinates(4385, -130, 1700),
									# # Coordinates(0, 3880, 850)],
                                    # 9.0,
                                    # 6.0,
                                    # 60.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# MussoPerformance = Performance([Coordinates(-4079, -5405, 1500),
                                    # Coordinates(4079, -5442, 400),
                                    # Coordinates(-4097, 5405, 2050),
                                    # Coordinates(4097, 5442, 900),],
									# # Coordinates(-4325, 0, 1800),
									# # Coordinates(0, -3980, 2000),
									# # Coordinates(4385, -130, 1700),
									# # Coordinates(0, 3880, 850)],
                                    # 6.0,
                                    # 8.0,
                                    # 60.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# MussoPerformance = Performance([Coordinates(-2040, -5405, 1500),
                                    # Coordinates(6119, -5442, 400),
                                    # Coordinates(-2049, 5405, 2050),
                                    # Coordinates(6146, 5442, 900),],
									# # Coordinates(-4325, 0, 1800),
									# # Coordinates(0, -3980, 2000),
									# # Coordinates(4385, -130, 1700),
									# # Coordinates(0, 3880, 850)],
                                    # 2.0,
                                    # 8.0,
                                    # 60.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# MussoPerformance = Performance([Coordinates(-4075, -5370, 1500),
                                    # Coordinates(11975, -5433, 400),
                                    # Coordinates(-4075, 5370, 2050),
                                    # Coordinates(11975, 5433, 900),],
									# # Coordinates(-4325, 0, 1800),
									# # Coordinates(0, -3980, 2000),
									# # Coordinates(4385, -130, 1700),
									# # Coordinates(0, 3880, 850)],
                                    # 5.0,
                                    # 8.0,
                                    # 60.0)
# # ---------- COPY PASTE END ----------
# ---------- COPY PASTE START ----------
# Modify Coordinates(X,Y,Z) and performance space X,Y and duration of each run here.
# Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# SIGPerformance = Performance([Coordinates(-1880 ,8710, 2210),
                                    # Coordinates(4040, 5400, 1700),
                                    # Coordinates(5190, -8370, 700),
                                    # Coordinates(-1880, -5111, 1000),],
									# # Coordinates(-4325, 0, 1800),
									# # Coordinates(0, -3980, 2000),
									# # Coordinates(4385, -130, 1700),
									# # Coordinates(0, 3880, 850)],
                                    # 9.60, # 4.80
                                    # 13.20, # 6.80
                                    # 60.0)
# ---------- COPY PASTE END ----------
# ---------- COPY PASTE START ----------
# Modify Coordinates(X,Y,Z) and performance space X,Y and duration of each run here.
# Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# SIGPerformance = Performance([Coordinates(-1800 ,10000, 2210),
#                                     Coordinates(4000, 6400, 1700),
#                                     Coordinates(5190, -6800, 700),
#                                     Coordinates(-1800, -3600, 1000),],
# 									# Coordinates(-4325, 0, 1800),
# 									# Coordinates(0, -3980, 2000),
# 									# Coordinates(4385, -130, 1700),
# 									# Coordinates(0, 3880, 850)],
#                                     9.60, # 4.80
#                                     13.20, # 6.80
#                                     60.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

# FluxumPerformance = Performance([Coordinates(5700 ,7010, 1300),
#                                    Coordinates(-5700, 7010, 3000),
#                                    Coordinates(-5700, -1820, 2100),
#                                    Coordinates(5500, -2860, 500),],
#                                    11.40,
#                                    5.50,
#                                    600.0)

# FluxumPerformance = Performance([Coordinates(5700 ,4935, 1300),
#                                     Coordinates(-5700, 4935, 3000),
#                                     Coordinates(-5700, -3895, 2100),
#                                     Coordinates(5500, -4935, 500),],
#                                     11.40,
#                                     9.80,
#                                     60.0)

# 3m x 3m small test space
# FluxumPerformance = Performance([Coordinates(4650 ,7450, 1300),
#                                     Coordinates(-6750, 7450, 3000),
#                                     Coordinates(-6750, -1380, 2100),
#                                     Coordinates(4450, -2420, 500),],
#                                     5.0,
#                                     5.0,
#                                     600.0)
# # ---------- COPY PASTE END ----------
# # ---------- COPY PASTE START ----------
# # Modify Coordinates(X,Y,Z) and performance space X,Y and duration of each run here.
# # Anchor order is sequential 1, 2, 3, 4 ... and should match InertiaMeter.py

SGG6emePerformance = Performance([Coordinates(4500 ,6900, 1500),
                                    Coordinates(-5500, 6900, 2200),
                                    Coordinates(-5500, -6900, 2400),
                                    Coordinates(4500, -6900, 1800),],
                                    4.0,
                                    4.0,
                                    600.0)

# # ---------- COPY PASTE END ----------
