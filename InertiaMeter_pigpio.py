#Class InertiaMeter
#   - Constructor(Performance)
#   - Output
#       - Location: distance to edge: North, South, East, West (taken from InertiMeter)
#       ??- LinearVelocity in m/s
#       - LinearAcceleration X, Y, Z
#       - AngularPosition Yaw, Pitch, Roll
#       - AngularVelocity Yaw, Pitch, Roll
#       ??- AngularAcceleration Yaw, Pitch, Roll


import time
import pigpio # http://abyz.co.uk/rpi/pigpio/python.html
from pypozyx import *
# from pypozyx.pozyx_serial import *
# from pypozyx.definitions.bitmasks import *
# from pypozyx.definitions.constants import *
# from pypozyx.definitions.registers import *
# from pypozyx.structures.device import *
# from pypozyx.structures.generic import Data, SingleRegister, dataCheck
# from pypozyx.structures.sensor_data import *

from Performance import Performance
import serial
#pins for pi

class InertiaMeter:

    def __init__(self, performance: Performance):

      
        #print(serial.tools.list_ports.comports()[0])
        ports = list(serial.tools.list_ports.comports())  

        # return the port if 'Pozyx' is in the description and serial number matches, in the future we should probably use something from the posyx_serial library               
        for port_no, description, address in ports:
            if 'Pozyx' in description and 'SER=316E388F3036 ' in address:
                self._serial_port = port_no
                print(self._serial_port)
        #self._serial_port = port_no       
        # self._serial_port_object = get_serial_ports()[i]
        # self._serial_port = self._serial_port_object.device
        # self._serial_port = "/dev/ttyACM2"
        
        self._pozyx = PozyxSerial(self._serial_port)
        #self.remote_id = remote_id
        
        self._anchors = [DeviceCoordinates(0x611a, 1, performance.inertiaMeterBeacon1),
                        DeviceCoordinates(0x610a, 1, performance.inertiaMeterBeacon2),
                        DeviceCoordinates(0x6110, 1, performance.inertiaMeterBeacon3),
                        DeviceCoordinates(0x611c, 1, performance.inertiaMeterBeacon4),]
						# DeviceCoordinates(0x683b, 1, performance.inertiaMeterBeacon5),
						# DeviceCoordinates(0x6826, 1, performance.inertiaMeterBeacon6),
						# DeviceCoordinates(0x682e, 1, performance.inertiaMeterBeacon7),
						# DeviceCoordinates(0x683a, 1, performance.inertiaMeterBeacon8),]

        # self._algorithm = POZYX_POS_ALG_UWB_ONLY  # positioning algorithm to use
        self._algorithm = POZYX_POS_ALG_TRACKING   # positioning algorithm to use
        self._dimension = POZYX_3D               # positioning dimension
        #self._position = Coordinates()
        #self.height = 350                      # height of device, required in 2.5D positioning
        self._pozyx.clearDevices()
        self._setAnchorsManual()
        self._pozyx.setPositionFilter(FILTER_TYPE_MOVINGAVERAGE, 15)
        self._pozyx.setRangingProtocol(POZYX_RANGE_PROTOCOL_PRECISION)
        
        
        # ms=0
        # ms = SingleRegister(ms, size=2)
        # data_size = 2
        # data_size = SingleRegister(data_size, size=2)
        # interval_status = self._pozyx.regWrite(PozyxRegisters.POZYX_POS_INTERVAL, ms)
        # #self._pozyx.setWrite(POZYX_POS_INTERVAL, ms)
        # print("Success:", str(self._pozyx.regRead(PozyxRegisters.POZYX_POS_INTERVAL, data_size)))
        
    def _initialisePozyx(self): 
         
        # self._serial_port_object.close()
         # #adapt these two
        # self._serial_port_object = get_serial_ports()[0]
        # self._serial_port = self._serial_port_object.device
        # print(self._serial_port)
        # self._pozyx = PozyxSerial(self._serial_port)
        
        self._pozyx.resetSystem()
        
        # self._pozyx.ser.close()
        # del self._pozyx
        # self._pozyx = None
        # ser = serial.Serial(self._serial_port)
        # ser.close()
        
        
        ports = list(serial.tools.list_ports.comports())  
        # return the port if 'Pozyx' is in the description and serial number matches, in the future we should probably use something from the posyx_serial library               
        for port_no, description, address in ports:
            if 'Pozyx' in description and 'SER=3185387D3036 ' in address:
                self._serial_port = port_no
        
        # self._serial_port_object = get_serial_ports()[0]
        # self._serial_port = self._serial_port_object.device
        # self._serial_port = "/dev/ttyACM2"
        
        print(self._serial_port)
        self._pozyx = PozyxSerial(self._serial_port)
                     
        self._pozyx.clearDevices()
        self._setAnchorsManual()
        self._pozyx.setPositionFilter(FILTER_TYPE_MOVINGAVERAGE, 15)
        self._pozyx.setRangingProtocol(POZYX_RANGE_PROTOCOL_PRECISION)
        print("done initialising")

    def _setAnchorsManual(self):
        self._status = self._pozyx.clearDevices(None)
        print(self._status)
        for self._anchor in self._anchors:
            # self._serial_port = get_serial_ports()[0].device
            self._status &= self._pozyx.addDevice(self._anchor, None)
            print("status:", self._status)
        # if len(self._anchors) > 4:
            # self._status &= self._pozyx.setSelectionOfAnchors(POZYX_ANCHOR_SEL_AUTO, len(self._anchors))      
            # self._pozyx.getNumberOfAnchors(nbr_anchors,None)
            # print("length" , ord(nbr_anchors[0]))
        print(self._status)
        return self._status
    
    @property
    def position(self)->Coordinates:
        try:
            # self._pozyx.ser.reset_input_buffer()
            
            self._position = Coordinates()
            for i in range(10):
                self._status = self._pozyx.doPositioning(self._position, self._dimension, 350, self._algorithm, None)
            if self._status == POZYX_SUCCESS:
                return self._position
                print("status:",self.status)
            else:
                
                self._initialisePozyx()
                self._position = Coordinates()
                self._status = self._pozyx.doPositioning(self._position, self._dimension, 350, self._algorithm, None)
                if self._status == POZYX_SUCCESS:
                    return self._position
                    #print("Done", POZYX_SUCCESS)        
                 
        except:
            raise Exception("position is not defined")
            
            
    @property
    def linearAcceleration(self)->Acceleration:
        try:
            self._linear_acceleration = Acceleration()
            self._status = self._pozyx.getAcceleration_mg(self._linear_acceleration, None)
            if self._status == POZYX_SUCCESS:
                #print("found device", self._serial_port)
                return self._linear_acceleration
                
            else:
                            
                self._initialisePozyx()
                self._position = Coordinates()
                self._status = self._pozyx.doPositioning(self._position, self._dimension, 350, self._algorithm, None)
                if self._status == POZYX_SUCCESS:
                    return self._position
                    #print("Done", POZYX_SUCCESS)                  
                                   
        except:
            raise Exception("linear_acceleration is not defined")
            
    @property
    def angularPosition(self)->EulerAngles:
        try:
            self._angular_position = EulerAngles()
            self._status = self._pozyx.getEulerAngles_deg(self._angular_position, None)
            if self._status == POZYX_SUCCESS:
                #print("found device", self._serial_port)
                return self._angular_position
                
            else:
                
                self._initialisePozyx()
                self._position = Coordinates()
                self._status = self._pozyx.doPositioning(self._position, self._dimension, 350, self._algorithm, None)
                if self._status == POZYX_SUCCESS:
                    return self._position
                    #print("Done", POZYX_SUCCESS)                   
                
        except:
            raise Exception("angular_position is not defined")

    @property
    def angularPositionQuaternion(self)->Quaternion:
        try:
            self.angular_position_quaternion = Quaternion()
            self._status = self._pozyx.getQuaternion(self.angular_position_quaternion, None)
            if self._status == POZYX_SUCCESS:
                #print("found device", self._serial_port)
                return self.angular_position_quaternion
                
            else:
                
                self._initialisePozyx()
                self._position = Coordinates()
                self._status = self._pozyx.doPositioning(self._position, self._dimension, 350, self._algorithm, None)
                if self._status == POZYX_SUCCESS:
                    return self._position
                    #print("Done", POZYX_SUCCESS)                   
                
        except:
            raise Exception("angular_position is not defined")			
			
            
    @property
    def angularVelocity(self)->AngularVelocity:
        try:
            self._angular_velocity = AngularVelocity()
            self._status = self._pozyx.getAngularVelocity_dps(self._angular_velocity, None)
            if self._status == POZYX_SUCCESS:
                return self._angular_velocity
            else:
                 
                self._initialisePozyx()
                self._position = Coordinates()
                self._status = self._pozyx.doPositioning(self._position, self._dimension, 350, self._algorithm, None)
                if self._status == POZYX_SUCCESS:
                    return self._position
                    #print("Done", POZYX_SUCCESS)
                    # print("trying to reconnect to pozyx...",self._status)  
                    # self._serial_port = get_serial_ports()[0].device
                    # print("found device", self._serial_port)
                    # self._pozyx = PozyxSerial(self._serial_port, timeout=30, write_timeout=30)
                    # print("Connecting")                     
        except:
            raise Exception("angular_velocity is not defined")
            
   
    @property
    def magneticOrientation(self)->Magnetic:
        try:
            self._magnetic_orientation = Magnetic()
            self._status = self._pozyx.getMagnetic_uT(self._magnetic_orientation, None)
            if self._status == POZYX_SUCCESS:
                #print("found device", self._serial_port)
                return self._magnetic_orientation
                
            else:
                
                self._initialisePozyx()
                self._position = Coordinates()
                self._status = self._pozyx.doPositioning(self._position, self._dimension, 350, self._algorithm, None)
                if self._status == POZYX_SUCCESS:
                    return self._position
                    #print("Done", POZYX_SUCCESS)                   
                
        except:
            raise Exception("angular_position is not defined")      
