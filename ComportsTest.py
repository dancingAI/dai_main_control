import serial
from serial.tools import list_ports

ports = list(serial.tools.list_ports.comports())  

# return the port if 'Pozyx' is in the description, in the future we should probably use something from the posyx_serial library
for port_no, description, address in ports:
    if 'Pozyx' in description :#and 'SER=316E388F3036 ' in address:
        print(port_no)
        print(description)
        print(address)
        #self._serial_port = port_no
    if 'Sabertooth' in description and ' SER=1600A83F67D6 ' in address:
        print(port_no)
        print(description)
        print(address)
        #self._serial_port = port_no
    if 'Sabertooth' in description and ' SER=1600A3A1E9A7 ' in address:
        print(port_no)
        print(description)
        print(address)
        #self._serial_port = port_no